﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.Net;
using MvcCurriculum.Models;

namespace MvcCurriculum.Controllers
{
    public class HomeController : Controller
    {
        
        //ATRIBUTOS DE CORREO
        private MailMessage datosCorreo = new MailMessage();
        private SmtpClient smtp = new SmtpClient();
        //ATRIBUTOS DE LA BASE DE DATOS
        private ReferenciaDb db = new ReferenciaDb();

        public ActionResult Index()
        {
           
            return View("DatosPersonales");
        }


        public ActionResult MostrarReferencias()
        {
            List<Referencia> referencias = db.Referencias.ToList();
            ViewBag.referencias = referencias;
            return View();
        }

        public ActionResult EnviarCorreo()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EnviarCorreo(string correoElectronico = "", string nombre = "", string asunto = "", string mensaje = "")
        {
            // TO ADD ES LA DIRECCION DONDE QUIERO QUE EL CORREO LLEGUE PUEDE SER GMAIL O HOTMAIL
            datosCorreo.To.Add(new MailAddress("progra.utn@gmail.com"));
            datosCorreo.From = new MailAddress(correoElectronico, nombre);
            datosCorreo.Subject = asunto;
            datosCorreo.Body = mensaje + "\n\n" + "Correo Electrónico: " + correoElectronico;

            smtp.Host = "smtp.gmail.com";
            //smtp.Host = "smtp.live.com";
            smtp.Port = 587;
            smtp.Credentials = new NetworkCredential("progra.utn@gmail.com", "pr1234567");
            smtp.EnableSsl = true;


            try
            {
                smtp.Send(datosCorreo);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View();
            }


        }


        public ActionResult DatosPersonales()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Home/Create

        [HttpPost]
        public ActionResult Create(string nombre = "", string telefono = "", string correo = "", string comentario = "")
        {
            Referencia nueva = new Referencia();

            nueva.Nombre = nombre;
            nueva.Telefono = telefono;
            nueva.Correo = correo;
            nueva.Comentario = comentario;

            if (ModelState.IsValid)
            {
                db.Referencias.Add(nueva);
                db.SaveChanges();
                return RedirectToAction("MostrarReferencias");
            }
            return View();

        }
                
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
