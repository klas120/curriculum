﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcCurriculum.Models
{
    public class Referencia
    {

        public int Id { get; set; }

        [Required(ErrorMessage = "Debe ingresar su nombre")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Debe ingresar su teléfono")]        
        public string Telefono { get; set; }

        [Required(ErrorMessage = "Debe ingresar su correo")]
        [EmailAddress(ErrorMessage = "El email no tiene el formato correcto")]
        public string Correo { get; set; }

        [Required(ErrorMessage = "Debe ingresar su comentario")]
        public string Comentario { get; set; }

    }
}