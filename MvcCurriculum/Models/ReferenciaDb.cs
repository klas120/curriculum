﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MvcCurriculum.Models
{
    public class ReferenciaDb : DbContext
    {
        public ReferenciaDb() : base("RecomencionConnection") 
        { }

        public DbSet<Referencia> Referencias { get; set;}
    }
}